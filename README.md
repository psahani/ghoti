![](ghoti.png)

**Ghoti** ([pronounced "fish"](https://en.wikipedia.org/wiki/Ghoti)) is a [game engine](https://en.wikipedia.org/wiki/Game_engine) written in C and [OpenGL](https://www.opengl.org/) featuring a [data-oriented](https://en.wikipedia.org/wiki/Data-oriented_design) [ECS (Enity-Component-System)](https://en.wikipedia.org/wiki/Entity_component_system), lightweight scripting in [Lua](https://lua.org/), and an advanced [physically-based renderer](https://en.wikipedia.org/wiki/Physically_based_rendering) with support for [HDR (High Dynamic Range)](https://en.wikipedia.org/wiki/High_dynamic_range) [environment maps](https://en.wikipedia.org/wiki/Reflection_mapping), [IBL (Image-Based Lighting)](https://en.wikipedia.org/wiki/Image-based_lighting), and a [BRDF (Bidirectional Reflectance Distribution Function)](https://en.wikipedia.org/wiki/Bidirectional_reflectance_distribution_function) based on previous models created by *Disney*, *Epic Games* (**[Unreal Engine 4](https://en.wikipedia.org/wiki/Unreal_Engine_4)**), and *Electronic Arts* (**[Frostbite](https://www.ea.com/frostbite/engine)**). The engine is [cross-platform](https://en.wikipedia.org/wiki/Cross-platform_software) and supports [x64](https://en.wikipedia.org/wiki/X86-64) Linux and Windows systems.

*A gold (metallic) material in a forest setting. The faint halo around the edges showcases support for accurate reflections according to the [Fresnel equations](https://en.wikipedia.org/wiki/Fresnel_equations):*

![](images/gold_material.png)

*A brick material by the water. The illusion of indented crevices is made possible through support for [parallax mapping](https://en.wikipedia.org/wiki/Parallax_mapping). The [hue](https://en.wikipedia.org/wiki/Hue) of the bricks matches the color of the environment thanks to support for IBL:*

![](images/brick_material.png)

## Associated Projects

This is a large project, so codebases for helper libraries and tools are separated into various other repositories:

- [Model Viewer](https://gitlab.com/psahani/model-viewer) - a standalone application for viewing and processing 3D models before importing them into the engine
- [Model Utility](https://gitlab.com/psahani/model-utility) - a CLI tool and shared library for exporting 3D model data to binary formats compatible with the engine
- [JSON Utilities](https://gitlab.com/psahani/json-utilities) - a set of CLI tools and a shared library for handling/serializing JSON files used in the ECS
- [File Utilities](https://gitlab.com/psahani/file-utilities) - a shared library containing common file management functions used by all of the projects

## Features

### Core

- An efficient ECS as a framework for designing and implementing [gameplay](https://en.wikipedia.org/wiki/Gameplay) features
- A [human-readable](https://en.wikipedia.org/wiki/Human-readable_medium_and_data) (JSON) [data format](https://en.wikipedia.org/wiki/File_format) for ECS objects which is automatically [serialized](https://en.wikipedia.org/wiki/Serialization) to an efficient [binary format](https://en.wikipedia.org/wiki/Binary_file) on-the-fly
- Lightweight, [dynamic](https://en.wikipedia.org/wiki/Dynamic_programming_language) scripting without recompilation using [LuaJIT](https://luajit.org/)
- Saving/loading of game state for [persistence](https://en.wikipedia.org/wiki/Persistence_(computer_science)) across play sessions
- An [error/message](https://en.wikipedia.org/wiki/Error_message) [logging system](https://en.wikipedia.org/wiki/Logging_(computing)) which supports both [console](https://en.wikipedia.org/wiki/Computer_terminal#System_console) output and logging to files
- [Rigid body dynamics](https://en.wikipedia.org/wiki/Rigid_body_dynamics) through the [Open Dynamics Engine](https://www.ode.org/) with support for hinge, slider, and ball-and-socket joints
- Box, sphere, and capsule [primitives](https://en.wikipedia.org/wiki/Collision_detection#Bounding_volumes) for [collision detection/resolution](https://en.wikipedia.org/wiki/Collision_detection)
- Rotation of objects using [quaternions](https://en.wikipedia.org/wiki/Quaternion) rather than [Euler angles](https://en.wikipedia.org/wiki/Euler_angles) to prevent [gimbal lock](https://en.wikipedia.org/wiki/Gimbal_lock) and to allow for [spherical linear interpolation](https://en.wikipedia.org/wiki/Slerp)
- Support for [deterministic simulation](https://en.wikipedia.org/wiki/Deterministic_system) and independent physics and graphics [frame rates](https://en.wikipedia.org/wiki/Frame_rate) using an [interpolated](https://en.wikipedia.org/wiki/Linear_interpolation) timestep (as described in [this blog post](https://gafferongames.com/post/fix_your_timestep/) by Glenn Fiedler)
- Various visualization tools for [debugging](https://en.wikipedia.org/wiki/Debugging) including display of object [transforms](https://en.wikipedia.org/wiki/Transformation_matrix) (translation, rotation, and scale), collision primitives, [model wireframes](https://en.wikipedia.org/wiki/Wire-frame_model), and arbitrary points and lines
- A [multithreaded](https://en.wikipedia.org/wiki/Multithreading_(computer_architecture)) asset management (loading/unloading) system with automatic [lifetime](https://en.wikipedia.org/wiki/Object_lifetime) tracking inspired by [garbage collection](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science))
- An interface to the [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) [immediate-mode](https://en.wikipedia.org/wiki/Immediate_mode_(computer_graphics)) [GUI toolkit](https://en.wikipedia.org/wiki/Widget_toolkit) for simple UI layouts and support for [font rendering](https://en.wikipedia.org/wiki/Font_rasterization)
- Mouse, keyboard, and controller input through [GLFW](https://www.glfw.org/) and [SDL2](https://www.libsdl.org/)
- Music/sound playback ([Ogg files](https://en.wikipedia.org/wiki/Ogg)) through [OpenAL](https://openal.org/)

*A simple UI layout with an interactive button scripted using Lua:*

![](videos/ui.mp4)

### Graphics

- Integration of the [Cook–Torrance BRDF model](https://en.wikipedia.org/wiki/Specular_highlight#Cook%E2%80%93Torrance_model) based on prior work done by *Disney*, *Epic Games* ([PDF](https://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf)), and *Electronic Arts* ([PDF](https://seblagarde.wordpress.com/wp-content/uploads/2015/07/course_notes_moving_frostbite_to_pbr_v32.pdf))
- HDR environment maps with support for IBL using precomputed [diffuse](https://en.wikipedia.org/wiki/Diffuse_reflection) [irradiance](https://en.wikipedia.org/wiki/Irradiance) maps and pre-filtered [specular reflections](https://en.wikipedia.org/wiki/Specular_reflection)
- [Gamma correction](https://en.wikipedia.org/wiki/Gamma_correction) as well as [HDR tone mapping](https://en.wikipedia.org/wiki/Tone_mapping)
- Support for [parallax mapping](https://en.wikipedia.org/wiki/Parallax_mapping), [normal mapping](https://en.wikipedia.org/wiki/Normal_mapping), and [ambient occlusion](https://en.wikipedia.org/wiki/Ambient_occlusion)
- Material masking (overlaying of multiple different materials onto a single [mesh](https://en.wikipedia.org/wiki/Polygon_mesh)) for [artistic control](https://en.wikipedia.org/wiki/Artistic_control) over decals and detailing of wear/erosion/foliage
- Directional and point lights with [anti-aliased](https://en.wikipedia.org/wiki/Spatial_anti-aliasing) soft shadows using [percentage-closer filtering](https://en.wikipedia.org/wiki/Texture_filtering#Percentage_Closer_filtering) and [shadow mapping](https://en.wikipedia.org/wiki/Shadow_mapping)
- Dynamic cameras with customizable attributes and support for both [orthographic](https://en.wikipedia.org/wiki/Orthographic_projection) and [perspective](https://en.wikipedia.org/wiki/Perspective_(graphical)) projection
- A custom [art/asset pipeline](https://en.wikipedia.org/wiki/Art_pipeline) supporting [FBX](https://en.wikipedia.org/wiki/FBX) models with [skeletal animations](https://en.wikipedia.org/wiki/Skeletal_animation) and collision geometry
- Support for basic [transitions/blending](https://en.wikipedia.org/wiki/Interpolation_(computer_graphics)) between different animations
- A [sprite-based](https://en.wikipedia.org/wiki/Sprite_(computer_graphics)) [particle system](https://en.wikipedia.org/wiki/Particle_system) using [texture atlases](https://en.wikipedia.org/wiki/Texture_atlas) and [geometry shaders](https://www.khronos.org/opengl/wiki/Geometry_Shader)
- [Back-face culling](https://en.wikipedia.org/wiki/Back-face_culling) as a basic optimization by default with support for double-sided meshes
- [MSAA (Multisample Anti-Aliasing)](https://en.wikipedia.org/wiki/Multisample_anti-aliasing) for smooth edges around objects

*A basic smoke particle effect and simple physics interactions in an [HDR holiday environment](https://polyhaven.com/a/christmas_photo_studio_01) by Sergej Majboroda:*

![](videos/particles.mp4)

## Releases

Compiled binaries for both Linux and Windows are available [here](https://gitlab.com/psahani/ghoti/-/releases/). There's also demo releases which include a simple example scene for testing.

## Development

### Useful Tools

- [LLDB](https://lldb.llvm.org/) - an open-source native debugger
- [Valgrind](https://valgrind.org/) - an open-source CLI tool which can be utilized for tracing memory leaks as well as profiling
- [RenderDoc](https://renderdoc.org/) - an invaluable open-source graphics debugger which was pervasively used throughout the project

### Building

#### Linux

Since care has been taken to include redistributable binaries of all necessary libraries in the `lib/` folder, all that should be needed to build the engine is to have [Clang](https://clang.llvm.org/) and [Make](https://www.gnu.org/software/make/) installed before running this single command:

```sh
$ make rebuild
```

The resulting binary will be placed in the `build/` folder by default.

Various other makefile targets are also available including `run` (runs the game), `release` (compiles a release version of the engine into the `release/` folder), `debug` (launches LLDB along with the game), and `leakcheck` (launches the game with Valgrind).

#### Windows

Building for Windows is only supported through [cross-compilation](https://en.wikipedia.org/wiki/Cross_compiler) from a Linux host using the [MinGW](https://www.mingw-w64.org/) compatibility environment. Redistributables are located in the `winlib/` folder. Assuming the `x86_64-w64-mingw32-clang` binary is available on the [system path](https://en.wikipedia.org/wiki/PATH_(variable)), simply run this command:

```sh
$ make windows
```

Release builds are also possible by setting the appropriate [environment variable](https://en.wikipedia.org/wiki/Environment_variable):

```sh
$ WINDOWS=true make release
```

The executable can be run on Linux using the [Wine](https://www.winehq.org/) compatibility layer if desired:

```sh
$ make wine
```

## Theory

This overview isn't an in-depth dive into the underlying theory behind the various features of the engine, but it lists the major theoretical ideas used within it, particularly focusing on the renderer.

The engine makes pervasive use of mathematical ideas from [linear algebra](https://en.wikipedia.org/wiki/Linear_algebra) and [vector calculus](https://en.wikipedia.org/wiki/Vector_calculus) for implementation of physics, gameplay, and graphics features.

The ultimate goal of the renderer, specifically, is to solve the [rendering equation](https://en.wikipedia.org/wiki/Rendering_equation), which models [equilibrium](https://en.wikipedia.org/wiki/Radiative_equilibrium) [radiance](https://en.wikipedia.org/wiki/Radiance) leaving a given point in space using ideas from [optics](https://en.wikipedia.org/wiki/Geometrical_optics) (namely [radiometry](https://en.wikipedia.org/wiki/Radiometry)):

![](images/rendering_equation.png)

To do so in [real-time](https://en.wikipedia.org/wiki/Real-time_simulation), approximation is necessary as an optimization. Naturally, this project makes use of many sophisticated [numerical methods](https://en.wikipedia.org/wiki/Numerical_method) commonly used for [light transport](https://en.wikipedia.org/wiki/Light_transport_theory) simulation. Solving the equation is split into separate steps. Consequently, all of the [pre-computation](https://en.wikipedia.org/wiki/Precomputation) necessary for IBL takes place beforehand through the model viewer. One can refer [here](https://gitlab.com/psahani/model-viewer/#theory) for an overview of the theory regarding that portion of the computation.

This renderer implements the Cook–Torrance BRDF which is comprised of a diffuse term and a specular term:

![](images/brdf.png)

The diffuse term is simply a constant (called [Lambertian diffuse](https://en.wikipedia.org/wiki/Lambertian_reflectance)):

![](images/brdf_diffuse.png)

while the specular term is as follows:

![](images/brdf_specular.png)

The **DFG** term is further broken down into three components, with each letter corresponding to separate functions, the normal **D**istribution function ([Trowbridge–Reitz](https://pharr.org/matt/blog/images/average-irregularity-representation-of-a-rough-surface-for-ray-reflection.pdf) [GGX approximation](https://www.cs.cornell.edu/~srm/publications/EGSR07-btdf.html)):

![](images/normal_distribution_function.png)

the **F**resnel equation ([Fresnel–Schlick approximation](https://en.wikipedia.org/wiki/Schlick's_approximation)):

![](images/fresnel_equation.png)

and the **G**eometry function (Smith's Schlick–GGX approximation):

![](images/geometry_function.png)

Including the full BRDF expands the general form of the rendering equation from before to the following final reflectance equation:

![](images/reflectance_equation.png)

All of these components are calculated in real-time within shaders with the help of the pre-computed results to compute the overall lighting for a scene.

## Implementation Notes

### Graphics

The [fragment shader](https://www.khronos.org/opengl/wiki/Fragment_Shader) `model_pbr.frag` in the `resources/shaders/` folder contains the bulk of the nontrivial GPU rendering code if one is interested in delving into the specifics of the rendering engine.

A number of advanced features were planned and/or researched yet unfortunately never got implemented due to time constraints including:

- A [Vulkan](https://www.vulkan.org/) backend for more precise control over the graphics pipeline
- A [deferred shading](https://en.wikipedia.org/wiki/Deferred_shading) pipeline
- Extended [post-processing](https://en.wikipedia.org/wiki/Video_post-processing) support through [convolution](https://en.wikipedia.org/wiki/Convolution) of matrix kernels for common effects such as [Gaussian blur](https://en.wikipedia.org/wiki/Gaussian_blur)
- Support for [motion blur](https://en.wikipedia.org/wiki/Motion_blur) using [temporal anti-aliasing](https://en.wikipedia.org/wiki/Temporal_anti-aliasing)
- Various rendering optimizations
  - [Frustum culling](https://en.wikipedia.org/wiki/Hidden-surface_determination#Viewing-frustum_culling)
  - [Cascaded shadow mapping](https://learnopengl.com/Guest-Articles/2021/CSM)
  - Dynamic adjustment of [LOD (Level of Detail)](https://en.wikipedia.org/wiki/Level_of_detail_(computer_graphics)) according to proximity
  - [Z-culling](https://en.wikipedia.org/wiki/Z-buffering#Z-culling)
  - [Texture compression](https://en.wikipedia.org/wiki/Texture_compression), especially for large high-resolution/HDR textures
- More complete support for [heightmaps](https://en.wikipedia.org/wiki/Heightmap) for rendering terrain
- [Inverse kinematics](https://en.wikipedia.org/wiki/Inverse_kinematics) for [animating characters more accurately](https://en.wikipedia.org/wiki/Inverse_kinematics#Inverse_kinematics_and_3D_animation)  when standing on top of varied terrain shapes
- Animation blend [trees](https://en.wikipedia.org/wiki/Tree_(abstract_data_type)) and [state machines](https://en.wikipedia.org/wiki/Finite-state_machine) for more sophisticated artistic/design control
- A [parallel](https://en.wikipedia.org/wiki/Parallel_computing) GPU-driven particle system implemented using [compute shaders](https://www.khronos.org/opengl/wiki/Compute_Shader)
- Support for creating custom shaders and attaching them individually to entities in the ECS
- Support for emissive materials with [bloom](https://en.wikipedia.org/wiki/Bloom_(shader_effect))
- Support for area lights
- Reflection probes for local IBL allowing accurate shifts in lighting between indoor and outdoor areas
- Enhanced support for [transparent/translucent](https://en.wikipedia.org/wiki/Transparency_and_translucency) materials using [alpha blending](https://en.wikipedia.org/wiki/Alpha_compositing)
- Integration of a [BSDF (Bidirectional Scattering Distribution Function)](https://en.wikipedia.org/wiki/Bidirectional_scattering_distribution_function) for materials requiring [subsurface scattering](https://en.wikipedia.org/wiki/Subsurface_scattering) such as ice or leaves
- Efficient real-time [global illumination](https://en.wikipedia.org/wiki/Global_illumination) through an implementation of [voxel](https://en.wikipedia.org/wiki/Voxel) [cone tracing](https://en.wikipedia.org/wiki/Cone_tracing) based on [this paper](https://research.nvidia.com/publication/2011-09_interactive-indirect-illumination-using-voxel-cone-tracing) published by *NVIDIA* and their implementation called **[VXGI](https://developer.nvidia.com/vxgi)**
  - This may now be an obsolete feature (outside of constrained devices such as phones) due to modern developments resulting in the advent of GPUs (such as *NVIDIA*'s **[GeForce RTX](https://www.nvidia.com/en-us/geforce/rtx/)**) with special built-in hardware instructions optimized for full [ray tracing](https://en.wikipedia.org/wiki/Ray_tracing_(graphics))

![](images/vxgi.png)

### Memory Management

This project made use of a naïve allocation strategy which involved pervasive use of `malloc()` as a global heap allocator paired with matching calls to `free()`. This ended up causing some tricky bugs related to [memory leaks/corruption](https://en.wikipedia.org/wiki/Memory_corruption) since such a strategy results in a complex memory ownership graph in large programs such as this one. A modern solution to this problem is to leverage programming languages which support either [linear or affine types](https://en.wikipedia.org/wiki/Substructural_type_system) for [statically](https://en.wikipedia.org/wiki/Type_system#Static_type_checking) tracking ownership through the type system. Although this solution can work, it usually comes at the cost of a significant increase in limitations regarding the types of ownership patterns that are safe to use. It also comes at the cost of slowdowns to productivity in the [prototyping](https://en.wikipedia.org/wiki/Software_prototyping) phase that the strictness of such a type system brings. A much simpler and more effective solution, especially for video games where memory access patterns and lifetimes are usually consistent and well-known (often acquired and released every frame), is the use of [arena allocators](https://en.wikipedia.org/wiki/Region-based_memory_management). These alleviate memory ownership issues at the source by limiting the total number of lifetimes in the program (by allocating chunks of memory in bulk instead) so that understanding and controlling the overarching ownership graph is much more tractable in large, complex systems.

Despite this broader mistake, however, a great success of this engine was its use of pre-allocation (mostly data tables for ECS components) and [cache-friendly](https://en.wikipedia.org/wiki/Locality_of_reference) memory layouts (primarily [SoA](https://en.wikipedia.org/wiki/Parallel_array) in the ECS). These tangibly resulted in faster and more efficient handling of large amounts of data/entities as confirmed through profiling in [benchmarks](https://en.wikipedia.org/wiki/Benchmark_(computing)) and [stress tests](https://en.wikipedia.org/wiki/Stress_testing).

[Time](https://en.wikipedia.org/wiki/Time_complexity) and [space](https://en.wikipedia.org/wiki/Space_complexity) [complexity analysis](https://en.wikipedia.org/wiki/Analysis_of_algorithms) only goes so far in terms of actually realized [algorithmic efficiency](https://en.wikipedia.org/wiki/Algorithmic_efficiency). A theoretically fast algorithm may be slow in practice due to poor implementation. As such, all angles are always necessary to consider if one wishes to create fast code in the real world. Memory is a [resource](https://en.wikipedia.org/wiki/Computational_resource) which naturally plays a very apparent role in whether an implementation approaches its theoretical maximum efficiency.

For further information on memory management, [here](https://verdagon.dev/grimoire/grimoire) is an excellent blog post by Evan Ovadia detailing various [manual memory management](https://en.wikipedia.org/wiki/Manual_memory_management) strategies and their relation to [memory safety](https://en.wikipedia.org/wiki/Memory_safety), particularly in the context of [systems programming languages](https://en.wikipedia.org/wiki/System_programming_language).

### Programming Languages

C and Lua were intentionally chosen for their [simplicity](https://en.wikipedia.org/wiki/Lightweight_programming_language), efficiency, and [portability](https://en.wikipedia.org/wiki/Software_portability) over other commonly used languages in this domain, such as C++ and Python, which often heavily rely on [subtyping](https://en.wikipedia.org/wiki/Subtyping) or [inheritance](https://en.wikipedia.org/wiki/Inheritance_(object-oriented_programming)) for [polymorphism](https://en.wikipedia.org/wiki/Polymorphism_(computer_science)). The lack of features in C and Lua were treated as benefits in that they forced development to focus on tangible output and velocity rather than creation of [abstractions](https://en.wikipedia.org/wiki/Abstraction_(computer_science)). Moreover, the simpler [semantics](https://en.wikipedia.org/wiki/Semantics_(computer_science)) of C and Lua gives developers a smaller design space to work with and allows for compilers (such as Clang/[LLVM](https://llvm.org/) and LuaJIT) to intelligently optimize common patterns much more heavily than in more complex languages. It's certainly possible to program in a simpler, more [procedural style](https://en.wikipedia.org/wiki/Procedural_programming) in languages like C++ or Python, but that means that one isn't fully utilizing the additional features of the language anyways. C++, specifically, has some features such as [operator overloading](https://en.wikipedia.org/wiki/Operator_overloading) which can be useful for increasing [readability](https://en.wikipedia.org/wiki/Readability) of mathematical code and implementing procedures using [SIMD](https://en.wikipedia.org/wiki/Single_instruction,_multiple_data), but the psychological distraction of the vast mountain of features in the language makes it tempting to get lost in search for the right subset of features to use, so sometimes it isn't worth the trade-off.

### Source Control

This project adheres to a simplified version of the Gitflow branching model as described [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow):

![](images/gitflow.png)

## Credits

This game engine was a 6-month-long, team-based university project created under the guidance of faculty together with two other students and in collaboration with other teams of students consisting of artists, game designers, gameplay developers, and software/tools developers.

*An overview of interfacing with the game engine through a graphical level editor built by another team of students:*

![](videos/level_editor.mp4)

Many thanks go out to the very detailed and thorough reference materials written by Joey de Vries at [learnopengl.com](https://learnopengl.com/) without which almost none of the implementation of advanced graphics features in this project would've been possible.

### External Libraries

- [Assimp](https://www.assimp.org/) - FBX file parsing
- [cJSON](https://github.com/DaveGamble/cJSON) - JSON file parsing
- [frozen](https://github.com/cesanta/frozen) - JSON file formatting/pretty-printing
- [GLEW](https://glew.sourceforge.net/) - OpenGL extensions
- [GLFW](https://www.glfw.org/) - window management
- [kazmath](https://github.com/Kazade/kazmath) - common linear algebra/math functions
- [LuaJIT](https://luajit.org/) - Lua scripting
- [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) - GUI, font rendering
- [Open Dynamics Engine](https://www.ode.org/) - physics
- [OpenAL](https://openal.org/) - sound and music
- [OpenGL](https://www.opengl.org/) - GPU-powered graphics
- [pthreads](https://en.wikipedia.org/wiki/Pthreads) - multithreading
- [SDL2](https://www.libsdl.org/) - controller input
- [stb_image/stb_vorbis](https://github.com/nothings/stb) - texture file parsing/Ogg file parsing